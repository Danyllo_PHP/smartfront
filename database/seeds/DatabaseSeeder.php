<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            [
                'name' => "Administrador"
            ],
            [
                'name' => "Fornecedor"
            ],
            [
                'name' => "Cliente"
            ]
        ]);
    }
}
