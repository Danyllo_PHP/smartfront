<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRoutesTable.
 */
class CreateRoutesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('routes', function(Blueprint $table) {
            $table->increments('id');
			$table->dateTime('date_start');
			$table->dateTime('date_end');
			$table->text('description')->nullable();
			$table->text('departure')->nullable();
			$table->text('destination')->nullable();
			$table->integer('author')->nullable();
			$table->integer('user_id');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('routes');
	}
}
