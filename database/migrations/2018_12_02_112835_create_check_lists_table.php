<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCheckListsTable.
 */
class CreateCheckListsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('check_lists', function(Blueprint $table) {
            $table->increments('id');
			$table->string('name',150);
			$table->dateTime('date_start');
			$table->dateTime('date_end');
			$table->time('duration')->nullable();
			$table->string('possible_points',50)->nullable();
			$table->string('points_made',50)->nullable();
			$table->string('device',100)->nullable();
			$table->integer('unconformities')->nullable();
			$table->integer('not_answered')->nullable();
			$table->integer('not_applicable')->nullable();
			$table->text('observation')->nullable();
			$table->integer('author');
			$table->integer('user_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->smallInteger('type')->default(0);
			$table->smallInteger('status')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('check_lists');
	}
}
