<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAtachmentsTable.
 */
class CreateAtachmentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachments', function(Blueprint $table) {
            $table->increments('id');
			$table->text('description')->nullable();
			$table->string('path', 150);
			$table->string('file', 150);
			$table->string('mime', 50)->nullable();
			$table->integer('attachmentable_id');
			$table->string('attachmentable_type', 30);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atachments');
	}
}
