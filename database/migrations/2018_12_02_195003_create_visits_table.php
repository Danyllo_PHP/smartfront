<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVisitsTable.
 */
class CreateVisitsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visits', function(Blueprint $table) {
            $table->increments('id');
			$table->dateTime('date_start');
			$table->dateTime('date_end');
			$table->text('description')->nullable();
			$table->smallInteger('author')->nullable();
			$table->smallInteger('user_id');
			$table->smallInteger('company_id');
			$table->smallInteger('route_id');
			$table->smallInteger('status');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visits');
	}
}
