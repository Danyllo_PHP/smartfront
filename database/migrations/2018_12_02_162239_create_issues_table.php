<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateIssuesTable.
 */
class CreateIssuesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table) {
            $table->increments('id');
			$table->smallInteger('status')->default(0);
			$table->smallInteger('check_list_id');
			$table->smallInteger('question_id');
			$table->integer('author')->nullable();
			$table->integer('user_id')->nullable();
			$table->text('observation')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issues');
	}
}
