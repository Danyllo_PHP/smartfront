<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateQuestionsTable.
 */
class CreateQuestionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions', function(Blueprint $table) {
            $table->increments('id');
			$table->text('question');
			$table->smallInteger('status');
			$table->smallInteger('check_list_id');
			$table->smallInteger('answer_id')->nullable();
			$table->smallInteger('answer')->default(0);
			$table->decimal('possible_points',10,2)->nullable();
			$table->decimal('points_made',10,2)->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions');
	}
}
