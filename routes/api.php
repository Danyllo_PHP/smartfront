<?php



Route::resource('users',     'UsersController', ['except' => ['create', 'edit']]);
Route::group(['middleware' => ['auth:api']], function () {
    
    Route::get('user-authenticated',  'UsersController@getUserAuthenticated');
    Route::get('address/{cep}',       'AddressesController@getAddress');
    Route::resource('users',          'UsersController', ['except' => ['create', 'edit']]);
    Route::resource('userTypes',      'UserTypesController', ['except' => ['create', 'edit']]);
    Route::resource('companies',      'CompaniesController', ['except' => ['create', 'edit']]);
    Route::resource('categories',     'CategoriesController', ['except' => ['create', 'edit']]);
    Route::resource('checkLists',     'CheckListsController', ['except' => ['create', 'edit']]);
    Route::resource('checkListTypes', 'CheckListTypesController', ['except' => ['create', 'edit']]);
    Route::resource('issues',         'IssuesController', ['except' => ['create', 'edit']]);
    Route::resource('providers',      'ProvidersController', ['except' => ['create', 'edit']]);
    Route::resource('questions',      'QuestionsController', ['except' => ['create', 'edit']]);
    Route::resource('routes',         'RoutesController', ['except' => ['create', 'edit']]);
    Route::resource('visits',         'VisitsController', ['except' => ['create', 'edit']]);
    Route::post('attachments',        'AttachmentsController@upload');

});
