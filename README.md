# Smart Api

Projeto de controle de ficalização e visita em Laravel 5.5.

## Laravel PHP Framework 5.5

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Passport
```
sudo chown www-data:www-data storage/oauth-*.key
sudo chmod 600 storage/oauth-*.key
```

## L5 Repository

:octocat: **[Github](https://github.com/andersao/l5-repository)**
Exemplo para gerar as classes automaticamente
```
php artisan make:entity Cliente --fillable="nome:string,email:string" --rules="nome=>required,email=>required|email"
```

## Limpar cache

```
php artisan cache:clear && php artisan config:clear && php artisan route:clear
```

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).