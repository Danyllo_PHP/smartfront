<?php

namespace App\Repositories;

use App\Presenters\CheckListTypePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CheckListTypeRepository;
use App\Entities\CheckListType;
use App\Validators\CheckListTypeValidator;

/**
 * Class CheckListTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CheckListTypeRepositoryEloquent extends AppRepository implements CheckListTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CheckListType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return CheckListTypeValidator::class;
    }

    /**
     * @return mixed
     */
   public function presenter()
   {
       return CheckListTypePresenter::class;
   }

}
