<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckListTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface CheckListTypeRepository extends RepositoryInterface
{
    //
}
