<?php

namespace App\Repositories;

use App\Presenters\RoutePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RouteRepository;
use App\Entities\Route;
use App\Validators\RouteValidator;

/**
 * Class RouteRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RouteRepositoryEloquent extends AppRepository implements RouteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Route::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RouteValidator::class;
    }

    /**
     * @return mixed
     */
    public function presenter()
    {
        return RoutePresenter::class;
    }

}
