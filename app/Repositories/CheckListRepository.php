<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckListRepository.
 *
 * @package namespace App\Repositories;
 */
interface CheckListRepository extends RepositoryInterface
{
    //
}
