<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VisitRepository.
 *
 * @package namespace App\Repositories;
 */
interface VisitRepository extends RepositoryInterface
{
    //
}
