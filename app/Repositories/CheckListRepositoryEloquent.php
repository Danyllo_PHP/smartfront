<?php

namespace App\Repositories;

use App\Presenters\CheckListPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CheckListRepository;
use App\Entities\CheckList;
use App\Validators\CheckListValidator;

/**
 * Class CheckListRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CheckListRepositoryEloquent extends AppRepository implements CheckListRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CheckList::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return CheckListValidator::class;
    }

    /**
     * @return mixed
     */
    public function presenter()
    {
        return CheckListPresenter::class;
    }

}
