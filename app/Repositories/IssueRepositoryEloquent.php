<?php

namespace App\Repositories;

use App\Presenters\IssuePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\IssueRepository;
use App\Entities\Issue;
use App\Validators\IssueValidator;

/**
 * Class IssueRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class IssueRepositoryEloquent extends AppRepository implements IssueRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Issue::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return IssueValidator::class;
    }

    /**
     * @return mixed
     */
    public function presenter()
    {
        return IssuePresenter::class;
    }


}
