<?php

namespace App\Repositories;

use App\Presenters\AnswerPresenter;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AnswerRepository;
use App\Entities\Answer;
use App\Validators\AnswerValidator;

/**
 * Class AnswerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AnswerRepositoryEloquent extends AppRepository implements AnswerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Answer::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return AnswerValidator::class;
    }

    /**
     * @return mixed
     */
   public function presenter()
   {
       return AnswerPresenter::class;
   }

}
