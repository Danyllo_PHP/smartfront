<?php

namespace App\Repositories;

use App\Presenters\QuestionPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuestionRepository;
use App\Entities\Question;
use App\Validators\QuestionValidator;

/**
 * Class QuestionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class QuestionRepositoryEloquent extends AppRepository implements QuestionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Question::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuestionValidator::class;
    }

    /**
     * @return mixed
     */
    public function presenter()
    {
        return QuestionPresenter::class;
    }

}
