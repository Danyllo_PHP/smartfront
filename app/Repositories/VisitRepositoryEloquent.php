<?php

namespace App\Repositories;

use App\Presenters\VisitPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\VisitRepository;
use App\Entities\Visit;
use App\Validators\VisitValidator;

/**
 * Class VisitRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VisitRepositoryEloquent extends AppRepository implements VisitRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Visit::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return VisitValidator::class;
    }

    /**
     * @return mixed
     */
    public function presenter()
    {
        return VisitPresenter::class;
    }
}
