<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Issue.
 *
 * @package namespace App\Entities;
 */
class Issue extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'check_list_id',
        'question_id',
        'author',
        'user_id',
        'observation',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_ACTIVE        = 0;
    const STATUS_SORTED_OUT    = 1;
}
