<?php

namespace App\Criterias;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterByAgencyCriteria
 * @package namespace App\Criteria;
 */
class FilterByCheckListCriteria extends AppCriteria implements CriteriaInterface
{

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $check_list_id = $this->request->query->get('check_list_id');

        if (is_numeric($check_list_id)) {
            $model = $model->where('check_list_id', $check_list_id);
        }
        return $model;
    }
}
