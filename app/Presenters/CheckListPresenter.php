<?php

namespace App\Presenters;

use App\Transformers\CheckListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CheckListPresenter.
 *
 * @package namespace App\Presenters;
 */
class CheckListPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CheckListTransformer();
    }
}
