<?php

namespace App\Presenters;

use App\Transformers\CheckListTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CheckListTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class CheckListTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CheckListTypeTransformer();
    }
}
