<?php

namespace App\Console;

use App\Entities\Client;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class testeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teste:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command verify new routes and give new permissions for Root Group';

    /**
     * teste constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        print_r(DB::connection('mysql')->table('fn_areceber')
            ->where('id_cliente', '=', 5)
            ->where('status', '=', 'A')
            ->select('id','id_cliente','status','data_vencimento')
            ->latest()->get());
    }
}
