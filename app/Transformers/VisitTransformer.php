<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Visit;

/**
 * Class VisitTransformer.
 *
 * @package namespace App\Transformers;
 */
class VisitTransformer extends TransformerAbstract
{
    /**
     * Transform the Visit entity.
     *
     * @param \App\Entities\Visit $model
     *
     * @return array
     */
    public function transform(Visit $model)
    {
        return [
            'id'         => (int) $model->id,
            'date_start'      => $model->date_start,
            'date_end'        => $model->date_end,
            'description'     => $model->description,
            'status'          => $model->status,
            'author'          => $model->author  ? $model->author : null,
            'user_id'         => $model->user_id ? $model->user_id : null,
            'company'         => $model->company_id ? $model->company : null,
            'route'           => $model->route_id ? $model->route : null,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
