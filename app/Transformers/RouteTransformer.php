<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Route;

/**
 * Class RouteTransformer.
 *
 * @package namespace App\Transformers;
 */
class RouteTransformer extends TransformerAbstract
{
    /**
     * Transform the Route entity.
     *
     * @param \App\Entities\Route $model
     *
     * @return array
     */
    public function transform(Route $model)
    {
        return [
            'id'              => (int) $model->id,
            'date_start'      => $model->date_start,
            'date_end'        => $model->date_end,
            'description'     => $model->description,
            'departure'       => $model->departure,
            'destination'     => $model->destination,
            'author'          => $model->author  ? $model->author : null,
            'user_id'         => $model->user_id ? $model->user_id : null,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
