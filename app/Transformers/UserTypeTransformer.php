<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\UserType;

/**
 * Class UserTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the UserType entity.
     *
     * @param \App\Entities\UserType $model
     *
     * @return array
     */
    public function transform(UserType $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name
        ];
    }
}
