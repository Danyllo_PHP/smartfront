<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\CheckList;

/**
 * Class CheckListTransformer.
 *
 * @package namespace App\Transformers;
 */
class CheckListTransformer extends TransformerAbstract
{
    /**
     * Transform the CheckList entity.
     *
     * @param \App\Entities\CheckList $model
     *
     * @return array
     */
    public function transform(CheckList $model)
    {
        return [
            'id'              => (int) $model->id,
            'name'            => $model->name,
            'possible_points' => $model->possible_points,
            'points_made'     => $model->points_made,
            'device'          => $model->device,
            'unconformities'  => $model->unconformities,
            'not_answered'    => $model->not_answered,
            'not_applicable'  => $model->not_applicable,
            'observation'     => $model->observation,
            'author'          => $model->author,
            'user_id'         => $model->user_id,
            'category_id'     => $model->category_id,
            'type'            => $model->type,
            'status'          => $model->status,
            'date_start'      => $model->date_start,
            'date_end'        => $model->date_end,
            'duration'        => $model->duration,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
