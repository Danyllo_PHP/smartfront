<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Provider;

/**
 * Class ProviderTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProviderTransformer extends TransformerAbstract
{
    /**
     * Transform the Provider entity.
     *
     * @param \App\Entities\Provider $model
     *
     * @return array
     */
    public function transform(Provider $model)
    {
        $address = null;
        if (count($model->address)) {
            $address = $model->address[0];
            $address = [
                'cep'          => $address->zip_code,
                'city'         => $address->city,
                'uf'           => $address->state,
                'street'       => $address->street,
                'neighborhood' => $address->district,
                'number'       => $address->number,
                'complement'   => $address->complement,
            ];
        }
        return [
            'id'              => (int) $model->id,
            'name'            => $model->name,
            'cpf_cnpj'        => $model->cpf_cnpj,
            'email'           => $model->email,
            'status'          => $model->status,
            'address'         => $address,
            'phone'           => $model->phone,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
