<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Attachment;

/**
 * Class AttachmentTransformer.
 *
 * @package namespace App\Transformers;
 */
class AttachmentTransformer extends TransformerAbstract
{
    /**
     * Transform the Attachment entity.
     *
     * @param \App\Entities\Attachment $model
     *
     * @return array
     */
    public function transform(Attachment $model)
    {
        return [
            'description'        => (int) $model->description,
            'path'               => $model->path,
            'file'               => $model->file,
            'mime'               => $model->mime,
            'attachmentable_id'  => $model->attachmentable_id,
            'attachmentable_type'=> $model->attachmentable_type,
            'created_at'         => $model->created_at->toDateTimeString(),
            'updated_at'         => $model->updated_at->toDateTimeString()
        ];
    }
}
