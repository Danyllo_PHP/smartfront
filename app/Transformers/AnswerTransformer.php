<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Answer;

/**
 * Class AnswerTransformer.
 *
 * @package namespace App\Transformers;
 */
class AnswerTransformer extends TransformerAbstract
{
    /**
     * Transform the Answer entity.
     *
     * @param \App\Entities\Answer $model
     *
     * @return array
     */
    public function transform(Answer $model)
    {
        return [
            'id'              => (int) $model->id,
            'answer'          => $model->answer,
            'status'          => $model->status,
            'check_list_id'   => $model->check_list_id,
            'question_id'     => $model->question_id,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
