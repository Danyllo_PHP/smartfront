<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Company;

/**
 * Class CompanyTransformer.
 *
 * @package namespace App\Transformers;
 */
class CompanyTransformer extends TransformerAbstract
{
    /**
     * Transform the Company entity.
     *
     * @param \App\Entities\Company $model
     *
     * @return array
     */
    public function transform(Company $model)
    {

        $address = null;
     
        if (count($model->addresses)) {
            $address = $model->addresses[0];
            $address = [
                'cep'          => $address->zip_code,
                'city'         => $address->city,
                'uf'           => $address->state,
                'street'       => $address->street,
                'neighborhood' => $address->district,
                'number'       => $address->number,
                'complement'   => $address->complement,
            ];
        }
        return [
            'id'              => (int) $model->id,
            'name'            => $model->name,
            'cpf_cnpj'        => $model->cpf_cnpj,
            'rg'              => $model->cpf_cnpj,
            'issuing_body'    => $model->issuing_body,
            'email'           => $model->email,
            'status'          => $model->status,
            'address'         => $address,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
