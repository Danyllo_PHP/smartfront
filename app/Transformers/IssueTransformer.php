<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Issue;

/**
 * Class IssueTransformer.
 *
 * @package namespace App\Transformers;
 */
class IssueTransformer extends TransformerAbstract
{
    /**
     * Transform the Issue entity.
     *
     * @param \App\Entities\Issue $model
     *
     * @return array
     */
    public function transform(Issue $model)
    {
        return [
            'id'              => (int) $model->id,
            'status'          => $model->status,
            'check_list_id'   => $model->check_list_id,
            'question_id'     => $model->question_id,
            'author'          => $model->author,
            'user_id'         => $model->user_id,
            'observation'     => $model->observation,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
