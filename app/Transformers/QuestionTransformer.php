<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Question;

/**
 * Class QuestionTransformer.
 *
 * @package namespace App\Transformers;
 */
class QuestionTransformer extends TransformerAbstract
{
    /**
     * Transform the Question entity.
     *
     * @param \App\Entities\Question $model
     *
     * @return array
     */
    public function transform(Question $model)
    {
        return [
            'id'              => (int) $model->id,
            'question'        => $model->question,
            'status'          => $model->status,
            'check_list_id'   => $model->check_list_id,
            'answer_id'       => $model->answer_id ? $model->answer_id : null,
            'answer'          => $model->answer,
            'possible_points' => $model->possible_points,
            'points_made'     => $model->points_made,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
