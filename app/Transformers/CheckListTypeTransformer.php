<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\CheckListType;

/**
 * Class CheckListTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class CheckListTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the CheckListType entity.
     *
     * @param \App\Entities\CheckListType $model
     *
     * @return array
     */
    public function transform(CheckListType $model)
    {
        return [
            'id'              => (int) $model->id,
            'name'            => $model->name,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString()
        ];
    }
}
