<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Validators\UserValidator;
use App\Http\Controllers\Traits\CrudMethods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    use CrudMethods;
    /**
     * @var UserService
     */
    protected $service;

    /**
     * @var UserValidator
     */
    protected $validator;

    /**
     * UsersController constructor.
     * @param UserService $service
     * @param UserValidator $validator
     */
    public function __construct(UserService $service, UserValidator $validator)
    {
        $this->service   = $service;
        $this->validator = $validator;
    }

    public function getUserAuthenticated()
    {
        return Auth::user();
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }
}
