<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Validators\CheckListTypeValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\CheckListTypeService;
/**
 * Class CheckListTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CheckListTypesController extends Controller
{
    use CrudMethods;
    /**
     * @var CheckListTypeService
     */
    protected $service;

    /**
     * @var CheckListTypeValidator
     */
    protected $validator;

    /**
     * CheckListTypesController constructor.
     * @param CheckListTypeService $service
     * @param CheckListTypeValidator $validator
     */
    public function __construct(CheckListTypeService $service, CheckListTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
