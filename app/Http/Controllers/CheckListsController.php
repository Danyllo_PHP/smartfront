<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\CheckListValidator;
use App\Services\CheckListService;
use App\Http\Controllers\Traits\CrudMethods;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CheckListsController.
 *
 * @package namespace App\Http\Controllers;
 */
class CheckListsController extends Controller
{
    use CrudMethods;
    /**
     * @var
     */
    protected $service;

    /**
     * @var CheckListValidator
     */
    protected $validator;

    /**
     * CheckListsController constructor.
     * @param CheckListService $service
     * @param CheckListValidator $validator
     */
    public function __construct(CheckListService $service, CheckListValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }

    public function confirm(Request $request)
    {
        return $this->service->confirm($request->all());
    }

}
