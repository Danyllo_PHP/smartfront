<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\ProviderValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\ProviderService;
use Illuminate\Http\Request;

/**
 * Class ProvidersController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProvidersController extends Controller
{
    use CrudMethods;
    /**
     * @var ProviderService
     */
    protected $service;

    /**
     * @var ProviderValidator
     */
    protected $validator;

    /**
     * ProvidersController constructor.
     * @param ProviderService $service
     * @param ProviderValidator $validator
     */
    public function __construct(ProviderService $service, ProviderValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }

}
