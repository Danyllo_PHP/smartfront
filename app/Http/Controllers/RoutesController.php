<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\RouteValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\RouteService;

/**
 * Class RoutesController.
 *
 * @package namespace App\Http\Controllers;
 */
class RoutesController extends Controller
{
    use CrudMethods;
    /**
     * @var RouteService
     */
    protected $service;

    /**
     * @var RouteValidator
     */
    protected $validator;

    /**
     * RoutesController constructor.
     * @param RouteService $service
     * @param RouteValidator $validator
     */
    public function __construct(RouteService $service, RouteValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }


}
