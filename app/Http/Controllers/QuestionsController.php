<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Repositories\QuestionRepository;
use App\Validators\QuestionValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\QuestionService;
use Illuminate\Http\Request;

/**
 * Class QuestionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuestionsController extends Controller
{
    use CrudMethods;
    /**
     * @var QuestionRepository
     */
    protected $service;

    /**
     * @var QuestionValidator
     */
    protected $validator;

    /**
     * QuestionsController constructor.
     * @param QuestionService $service
     * @param QuestionValidator $validator
     */
    public function __construct(QuestionService $service, QuestionValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }



    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }
}
