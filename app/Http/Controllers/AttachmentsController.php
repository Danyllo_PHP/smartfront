<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\AttachmentValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AttachmentService;
use Illuminate\Http\Request;

/**
 * Class AttachmentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AttachmentsController extends Controller
{
    use CrudMethods;
    /**
     * @var AttachmentService
     */
    protected $service;

    /**
     * @var AttachmentValidator
     */
    protected $validator;

    /**
     * AttachmentsController constructor.
     * @param AttachmentService $service
     * @param AttachmentValidator $validator
     */
    public function __construct(AttachmentService $service, AttachmentValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

    public function upload(Request $request)
    {
        try {

            if ($request->file('attachment')) {

                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));

                // Recupera a extensão do arquivo
                $extension = $request->file('attachment')->extension();

                // Define finalmente o nome
                $nameFile = "{$name}.{$extension}";

                // Faz o upload:
               return $upload = $request->file('attachment')->storeAs('attachments', $nameFile);

            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}
