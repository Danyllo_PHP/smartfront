<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Validators\UserTypeValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\UserTypeService;
/**
 * Class UserTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class UserTypesController extends Controller
{
    use CrudMethods;
    /**
     * @var UserTypeService
     */
    protected $service;

    /**
     * @var UserTypeValidator
     */
    protected $validator;

    /**
     * UserTypesController constructor.
     * @param UserTypeService $service
     * @param UserTypeValidator $validator
     */
    public function __construct(UserTypeService $service, UserTypeValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
