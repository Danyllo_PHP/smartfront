<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\AnswerRepository;
use App\Validators\AnswerValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\AnswerService;

/**
 * Class AnswersController.
 *
 * @package namespace App\Http\Controllers;
 */
class AnswersController extends Controller
{
    use CrudMethods;
    /**
     * @var AnswerRepository
     */
    protected $service;

    /**
     * @var AnswerValidator
     */
    protected $validator;

    /**
     * AnswersController constructor.
     * @param AnswerService $service
     * @param AnswerValidator $validator
     */
    public function __construct(AnswerService $service, AnswerValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

}
