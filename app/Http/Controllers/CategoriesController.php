<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\CategoryValidator;
use App\Services\CategoriesService;
use App\Http\Controllers\Traits\CrudMethods;
use Illuminate\Http\Request;

/**
 * Class CategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CategoriesController extends Controller
{
    use CrudMethods;
    /**
     * @var CategoriesService
     */
    protected $service;

    /**
     * @var CategoryValidator
     */
    protected $validator;

    /**
     * CategoriesController constructor.
     * @param CategoriesService $service
     * @param CategoryValidator $validator
     */
    public function __construct(CategoriesService $service, CategoryValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }

}
