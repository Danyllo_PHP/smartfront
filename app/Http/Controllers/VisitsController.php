<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\VisitValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\VisitService;
use Illuminate\Http\Request;

/**
 * Class VisitsController.
 *
 * @package namespace App\Http\Controllers;
 */
class VisitsController extends Controller
{
    use CrudMethods;
    /**
     * @var VisitService
     */
    protected $service;

    /**
     * @var VisitValidator
     */
    protected $validator;

    /**
     * VisitsController constructor.
     * @param VisitService $service
     * @param VisitValidator $validator
     */
    public function __construct(VisitService $service, VisitValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }


    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }
}
