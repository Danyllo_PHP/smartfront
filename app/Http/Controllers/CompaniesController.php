<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\CompanyValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\CompanyService;
use Illuminate\Http\Request;

/**
 * Class CompaniesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CompaniesController extends Controller
{
    use CrudMethods;
    /**
     * @var
     */
    protected $repository;

    /**
     * @var CompanyValidator
     */
    protected $validator;

    /**
     * CompaniesController constructor.
     * @param CompanyService $service
     * @param CompanyValidator $validator
     */
    public function __construct(CompanyService $service, CompanyValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }
    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }
  
}
