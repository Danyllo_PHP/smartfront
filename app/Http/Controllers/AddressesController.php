<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Services\AddressService;
use App\Validators\AddressValidator;
use App\Http\Controllers\Traits\CrudMethods;
/**
 * Class AddressesController.
 *
 * @package namespace App\Http\Controllers;
 */
class AddressesController extends Controller
{
    use CrudMethods;
    /**
     * @var $service
     */
    protected $service;

    /**
     * @var AddressValidator
     */
    protected $validator;

    /**
     * AddressesController constructor.
     * @param AddressService $service
     * @param AddressValidator $validator
     */
    public function __construct(AddressService $service, AddressValidator $validator)
    {
        $this->service    = $service;
        $this->validator  = $validator;
    }

    /**
     * @param $cep
     * @return array
     * @throws \Exception
     */
    public function getAddress($cep)
    {
        return $this->service->getAddress($cep);
    }

}
