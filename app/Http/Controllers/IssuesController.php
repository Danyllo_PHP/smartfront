<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Validators\IssueValidator;
use App\Http\Controllers\Traits\CrudMethods;
use App\Services\IssueService;
use Illuminate\Http\Request;

/**
 * Class IssuesController.
 *
 * @package namespace App\Http\Controllers;
 */
class IssuesController extends Controller
{
    use CrudMethods;
    /**
     * @var $service
     */
    protected $service;

    /**
     * @var IssueValidator
     */
    protected $validator;

    /**
     * IssuesController constructor.
     * @param IssueService $service
     * @param IssueValidator $validator
     */
    public function __construct(IssueService $service, IssueValidator $validator)
    {
        $this->service   = $service;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function updateStatus(Request $request, $id)
    {
        return $this->service->updateStatus($request->get('status'), $id);
    }
}

