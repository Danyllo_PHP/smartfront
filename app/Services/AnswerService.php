<?php

namespace App\Services;

use App\Repositories\AnswerRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class AnswerService
 * @package App\Services
 */
class AnswerService extends AppService
{
    use CrudMethods;

    /**
     * @var AnswerRepository
     */
    protected $repository;

    public function __construct(AnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    
}