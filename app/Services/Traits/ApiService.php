<?php

namespace App\Services\Traits;

use GuzzleHttp\Client;


/**
 * Class CrudMethods
 * @package app\Services\Traits
 */
trait ApiService
{
    /**
     * Http communication client
     *
     * @var object GuzzleHttp\Client
     */
    protected $httpClient;
    /**
     * @var
     */
    protected $config;

    /**
     * Get the base url
     *
     * @return string $baseUrl
     */
    public function getBaseUrl()
    {
        return config('crawler.url');
    }


    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    private function getHttpClient()
    {
        if ($this->httpClient == null)
        {
            $this->httpClient = new Client();
        }

        return $this->httpClient;

    }

    /**
     * Mount GET params to the request
     *
     * @param array $params
     * @return string
     */
    protected function httpQueryBuild(array $params)
    {
        $httpQuery = '';

        foreach ($params as $key => $value)
        {
            if (!is_array($value))
            {
                $httpQuery .= urlencode($key)  .'='. urlencode($value) . '&';
            } else {
                foreach ($value as $v2)
                {
                    if (!is_array($v2))
                    {
                        $httpQuery .= urlencode($key)  .'='. urlencode($v2) . '&';
                    }
                }
            }
        }

        $httpQuery = rtrim($httpQuery, '&');

        return '?' . $httpQuery;
    }



}