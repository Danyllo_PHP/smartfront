<?php

namespace App\Services;

use App\Repositories\AttachmentRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class AnswerService
 * @package App\Services
 */
class AttachmentService extends AppService
{
    use CrudMethods;

    /**
     * @var AttachmentRepository
     */
    protected $repository;

    public function __construct(AttachmentRepository $repository)
    {
        $this->repository = $repository;
    }

    
}