<?php

namespace App\Services;

use App\Entities\CheckList;
use App\Presenters\IssuePresenter;
use App\Repositories\CheckListRepository;
use App\Services\Traits\CrudMethods;
use App\Services\QuestionService;
use App\Services\IssueService;
/**
 * Class CheckListService
 * @package App\Services
 */
class CheckListService extends AppService
{
    use CrudMethods{
        all as public processAll;
        create as public processCreate;
    }

    /**
     * @var CheckListRepository
     */
    protected $repository;
    /**
     * @var
     */
    protected $questionService;

    protected  $issueService;

    /**
     * CheckListService constructor.
     * @param CheckListRepository $repository
     * @param \App\Services\QuestionService $questionService
     * @param \App\Services\IssueService $issueService
     */
    public function __construct(CheckListRepository $repository,
                                QuestionService $questionService,
                                IssueService $issueService)
    {
        $this->repository      = $repository;
        $this->questionService = $questionService;
        $this->issueService    = $issueService;
    }


    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $user = Service::getUser(true);
        $data['author'] = $user->id;
        return $this->processCreate($data);
    }

    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }

    /**
     * @param array $data
     * @return array|mixed
     */
    public function confirm(array $data)
    {
        $questions = $data['questions'];
        $possible_points = 0;
        $points_made     = 0;
        $unconformities  = [];
        foreach ($questions as $question) {
           $possible_points =  $possible_points + $question->possible_points;
            if ($question->answer == 0){
                $unconformities[] = $question;
                $points_made = $possible_points - $question->possible_points;
            }
        }

        $status = count($unconformities) ? CheckList::STATUS_BLOCKED : CheckList::STATUS_CONFIRMED;
        if ( count($unconformities)){
            foreach ($unconformities as $unconformity) {
                $dataIssue = [
                    'check_list_id' =>  $data['id'],
                    'question_id'   =>  $unconformity['id'],
                    'status'        =>  0,
                ];
                $this->issueService->create($dataIssue);
            }
            
        }
       return $this->update([
           'possible_points' => $possible_points,
           'points_made'     => $points_made ,
           'status'          => $status,
           'device'          => $data['device'],
           'observation'     => $data['observation'],
           'unconformities'  => !count($unconformities) ? 0 : count($unconformities)],
           $data['id']);
    }
    
}