<?php

namespace App\Services;

use App\Repositories\RouteRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class RouteService
 * @package App\Services
 */
class RouteService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var RouteRepository
     */
    protected $repository;

    /**
     * RouteService constructor.
     * @param RouteRepository $repository
     */
    public function __construct(RouteRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }
    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }


}