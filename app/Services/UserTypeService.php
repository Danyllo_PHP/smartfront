<?php

namespace App\Services;

use App\Repositories\UserTypeRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class RouteService
 * @package App\Services
 */
class UserTypeService extends AppService
{
    use CrudMethods;

    /**
     * @var UserTypeRepository
     */
    protected $repository;

    /**
     * UserTypeService constructor.
     * @param UserTypeRepository $repository
     */
    public function __construct(UserTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    
}