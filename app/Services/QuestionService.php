<?php

namespace App\Services;

use App\Repositories\QuestionRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class IssueService
 * @package App\Services
 */
class QuestionService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var QuestionRepository
     */
    protected $repository;

    /**
     * QuestionService constructor.
     * @param QuestionRepository $repository
     */
    public function __construct(QuestionRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('App\Criterias\FilterByCheckListCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status], $id);
    }

}