<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class AnswerService
 * @package App\Services
 */
class CategoriesService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * CategoriesService constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }


    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }


}