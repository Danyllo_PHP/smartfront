<?php

namespace App\Services;

use App\Entities\User;
use App\Repositories\UserRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class BankAccountService
 * @package App\Services
 */
class UserService extends AppService
{
    use CrudMethods {
        create as public processCreate;
        update as public processUpdate;
    }

    /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $data, $skipPresenter = false) 
    {
        $data = [
			'name'     => $data['name'],
			'email'    => $data['email'],
            'password' => bcrypt($data['password']),
            'type_id'  => $data["type_id"]
        ];
        
        return $skipPresenter ? $this->repository->skipPresenter()->create($data) : $this->repository->create($data);
    }

    
}