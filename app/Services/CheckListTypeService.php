<?php

namespace App\Services;

use App\Repositories\CheckListTypeRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class CheckListService
 * @package App\Services
 */
class CheckListTypeService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var CheckListTypeRepository
     */
    protected $repository;

    /**
     * CheckListTypeService constructor.
     * @param CheckListTypeRepository $repository
     */
    public function __construct(CheckListTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    
}