<?php

namespace App\Services;

use App\Entities\Address;
use App\Repositories\CompanyRepository;
use App\Services\AddressService;
use App\Services\Traits\CrudMethods;

/**
 * Class CheckListService
 * @package App\Services
 */
class CompanyService extends AppService
{
    use CrudMethods {
        create as public processCreate;
        update as public processUpdate;
        all    as public processAll;
    }

    /**
     * @var CompanyRepository
     */
    protected $repository;
    /**
     * @var \App\Services\AddressService
     */
    protected $addressService;

    /**
     * CompanyService constructor.
     * @param CompanyRepository $repository
     * @param \App\Services\AddressService $addressService
     */
    public function __construct(CompanyRepository $repository, AddressService $addressService)
    {
        $this->repository    = $repository;
        $this->addressService = $addressService;
    }

    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        $company = $this->processCreate($data);
        if (!empty($data['address'])) {
            $address      = $data['address'];
            $data_address = [
                'zip_code'         => $this->clearMask($address['zip_code']),
                'country'          => 'BR',
                'state'            => $address['state'],
                'district'         => $address['district'],
                'number'           => $address['number'] ? $address['number'] : null,
                'city'             => $address['city'],
                'street'           => $address['street'],
                'complement'       => $address['complement'],
                'addressable_id'   => $company['data']['id'],
                'addressable_type' => Address::ADDRESS_TYPE_COMPANY,
            ];
            $this->addressService->create($data_address);
        }
    }

    /**
     * @param array $data
     * @param $id
     * @return array|mixed
     */
    public function update(array $data ,$id)
    {
        $company = $this->processUpdate($data,$id);
        if (!empty($data['address'])) {
            $address      = $data['address'];
            $data_address = [
                'zip_code'         => $this->clearMask($address['zip_code']),
                'country'          => 'BR',
                'state'            => $address['state'],
                'district'         => $address['district'],
                'number'           => $address['number'] ? $address['number'] : null,
                'city'             => $address['city'],
                'street'           => $address['street'],
                'complement'       => $address['complement'],
                'addressable_id'   => $id,
                'addressable_type' => Address::ADDRESS_TYPE_COMPANY,
            ];
            $this->addressService->update($data_address, $id);
        }
        return $company;
    }


    /**
     * @param $string
     * @return mixed
     */
    public function clearMask($string)
    {
        $vowels = [ ":","(", ")", "+", "-", "*", "_", "."];
        return str_replace($vowels, "", $string);
    }

    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }

}