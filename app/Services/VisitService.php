<?php

namespace App\Services;

use App\Repositories\VisitRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class RouteService
 * @package App\Services
 */
class VisitService extends AppService
{
    use CrudMethods{
        create as public processCreate;
        all as public processAll;
    }

    /**
     * @var VisitRepository
     */
    protected $repository;

    /**
     * VisitService constructor.
     * @param VisitRepository $repository
     */
    public function __construct(VisitRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('App\Criterias\FilterByUserCriteria'))
            ->pushCriteria(app('App\Criterias\FilterByCompanyCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $user = Service::getUser(true);
        $data['author'] = $user->id;
        return $this->processCreate($data);
    }
    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }
    
}