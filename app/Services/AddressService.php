<?php

namespace App\Services;

use App\Repositories\AddressRepository;
use App\Services\Traits\CrudMethods;
use GuzzleHttp\Client;
/**
 * Class BankAccountService
 * @package App\Services
 */
class AddressService extends AppService
{
    use CrudMethods {
        create as public processCreate;
        update as public processUpdate;
    }
    private $client;
    /**
     * @var AddressRepository
     */
    protected $repository;

    public function __construct(AddressRepository $repository, Client $client)
    {
        $this->repository = $repository;
        $this->client = $client;
    }

    /**
     * @param $data
     * @param $id
     * @return array|mixed
     */
    public function update($data, $id)
    {
        $address = $this->findWhere(['addressable_id' => $id, 'addressable_type' => $data['addressable_type']], true);
        if (empty($address)) {
            return $this->processCreate($data);
        }
        return $this->processUpdate($data, $address->id);
    }

    /**
     * @param $cep
     * @return array
     * @throws \Exception
     */
    public function getAddress($cep)
    {
        $url = 'https://viacep.com.br/ws/';
        $cep = preg_replace("/[.\/-]/", '', $cep);
        $res = $this->client->request('GET', $url.$cep . '/json/');
        $response =(object) json_decode($res->getBody(), true);
        if(empty($response)){
            if (!$response) {
                throw new \Exception('Invalid response');
            }else{
                throw new \Exception("CEP Inválido!");
            }
        }

        if(isset($response->erro) && $response->erro){
            throw new \Exception("CEP não encontrado!");
        }

        return [
            'zip_code'    => $cep,
            'street'      => $response->logradouro,
            'district'    => $response->bairro,
            'city'        => $response->localidade,
            'state'       => $response->uf,
            'street_view' => 'maps.google.co.in/maps?q='.$cep
        ];

    }
}