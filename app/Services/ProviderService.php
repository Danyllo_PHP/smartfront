<?php

namespace App\Services;

use App\Entities\Address;
use App\Repositories\ProviderRepository;
use App\Services\Traits\CrudMethods;
use App\Services\AddressService;
/**
 * Class IssueService
 * @package App\Services
 */
class ProviderService extends AppService
{
    use CrudMethods {
        create as public processCreate;
        update as public processUpdate;
        all    as public processAll;
    }


    /**
     * @var IssueRepository
     */
    protected $repository;
    /**
     * @var \App\Services\AddressService
     */
    protected $addressService;
    /**
     * ProviderService constructor.
     * @param ProviderRepository $repository
     */
    public function __construct(ProviderRepository $repository,AddressService $addressService)
    {
        $this->repository = $repository;
        $this->addressService =$addressService;
    }

    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $company = $this->processCreate($data);
        if (!empty($data['address'])) {
            $address      = $data['address'];
            $data_address = [
                'zip_code'         => $this->clearMask($address['zip_code']),
                'country'          => 'BR',
                'state'            => $address['state'],
                'district'         => $address['district'],
                'number'           => $address['number'] ? $address['number'] : null,
                'city'             => $address['city'],
                'street'           => $address['street'],
                'complement'       => $address['complement'],
                'addressable_id'   => $company['data']['id'],
                'addressable_type' => Address::ADDRESS_TYPE_PROVIDER,
            ];
            $this->addressService->create($data_address);
        }
        return $company;
    }

    /**
     * @param array $data
     * @param $id
     * @return array|mixed
     */
    public function update(array $data ,$id)
    {
        $provider = $this->processUpdate($data, $id);
        if (!empty($data['address'])) {
            $address      = $data['address'];
            $data_address = [
                'zip_code'         => $this->clearMask($address['zip_code']),
                'country'          => 'BR',
                'state'            => $address['state'],
                'district'         => $address['district'],
                'number'           => $address['number'] ? $address['number'] : null,
                'city'             => $address['city'],
                'street'           => $address['street'],
                'complement'       => $address['complement'],
                'addressable_id'   => $id,
                'addressable_type' => Address::ADDRESS_TYPE_PROVIDER,
            ];
            $this->addressService->update($data_address, $id);
        }
        return $provider;
    }

    /**
     * @param $string
     * @return mixed
     */
    public function clearMask($string)
    {
        $vowels = [ ":","(", ")", "+", "-", "*", "_", "."];
        return str_replace($vowels, "", $string);
    }


    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }


}