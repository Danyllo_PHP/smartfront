<?php

namespace App\Services;
use App\Services\Traits\ApiService;
use GuzzleHttp\Exception\ClientException;

/**
 * Class AppService
 * @package App\Services
 */
class AppService
{
    use ApiService;
    /**
     * Http communication client
     *
     * @var object GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * AppService constructor.
     */
    public function __construct()
    {
        $this->httpClient = $this->getHttpClient();
    }

    /**
     * @param $resource
     * @param $params
     * @param $method
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|mixed|\Psr\Http\Message\ResponseInterface|\Symfony\Component\HttpFoundation\Response
     */
    public function request($resource, $params, $method)
    {
        try {
            $urlParams = '';

            if($method == 'GET'){
                $urlParams = $params;
                $params = [];
            }

            $endpoint = $this->getBaseUrl() . $resource . $urlParams;

            $options = [
                'headers' => [
                    'Accept' => '*/*',
                ],
                'form_params' => $params,
            ];

            $response = $this->getHttpClient()->request($method, $endpoint, $options);

            if ($response->getHeader('Content-Type')[0] == "application/pdf") {
                $pdf = (string) $response->getBody()->getContents();

                return response($pdf , 200, [
                    'Content-Type' 				    => 'application/pdf',
                    'Access-Control-Expose-Headers' => 'File-Name',
                    'File-Name' 					=> 'ticket.pdf'
                ]);
            } else {
                $response = json_decode($response->getBody(), true);
            }

        } catch (ClientException $e) {
            $message = json_decode($e->getResponse()->getBody(), true);
            return response()->json($message, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {

            return response()->json([
                "error" => true,
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
            ]);
        }

        return $response;
    }

}