<?php

namespace App\Services;

use App\Repositories\IssueRepository;
use App\Services\Traits\CrudMethods;

/**
 * Class IssueService
 * @package App\Services
 */
class IssueService extends AppService
{
    use CrudMethods{
        all as public processAll;
    }

    /**
     * @var IssueRepository
     */
    protected $repository;

    /**
     * IssueService constructor.
     * @param IssueRepository $repository
     */
    public function __construct(IssueRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * @param $status
     * @param $id
     * @return array|mixed
     */
    public function updateStatus($status, $id)
    {
        return $this->update(['status '=> $status],$id);
    }

    /**
     * Display a listing of the resource.
     * @param $limit
     * @return array|mixed
     */
    public function all($limit)
    {
        $this->repository
            ->resetCriteria()
            ->pushCriteria(app('App\Criterias\FilterByStatusCriteria'))
            ->pushCriteria(app('App\Criterias\FilterByCheckListCriteria'))
            ->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        return $this->processAll($limit);

    }

    
}